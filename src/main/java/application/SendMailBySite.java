package application;

import java.util.List;
import java.util.Properties;  
import javax.mail.*;  
import javax.mail.internet.*;


import bean.ReceiverAccount;
import bean.SenderAccount;
import utility.Convert;  

public class SendMailBySite {  
	private MimeMessage message; 
	private Session session;
	public void mailSent(SenderAccount sender,List<ReceiverAccount> receivers,String subject,String textMessage) throws Exception{
		createSession(sender);
		createMessage(sender, receivers, subject, textMessage);
		Transport.send(message);  

	}


	private void createSession(SenderAccount senderAccount){
		final String user=senderAccount.getUserName();
		final String password=senderAccount.getPassword();
		String host="mail2.mclink.it";  
		//Get the session object  
		Properties props = new Properties();  
		props.put("mail.smtp.host",host);  
		props.put("mail.smtp.auth", "true");  
		session = Session.getDefaultInstance(props,  
				new javax.mail.Authenticator() {  
			protected PasswordAuthentication getPasswordAuthentication() {  
				return new PasswordAuthentication(user,password);  
			}
		});
	}

	private void createMessage(SenderAccount senderAccount,List<ReceiverAccount> receivers,String subject,String textMessage) throws Exception, MessagingException{
		message = new MimeMessage(session);  
		message.setFrom(new InternetAddress(senderAccount.getUserName()));
		if(subject.contains("FAILED")){
			message.setHeader("X-Priority", "1");
		}
		message.addRecipients(Message.RecipientType.TO, Convert.receiverAccountsToAddresses(receivers));
		message.setSubject(subject);  
		message.setText(textMessage);
	}
}  