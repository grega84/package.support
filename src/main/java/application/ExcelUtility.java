package application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import bean.ScenarioDetails;

public class ExcelUtility {
	private XSSFWorkbook workbook; 
    private CreationHelper createHelper;
    private XSSFSheet sheet; 
    
    public static void main(String[] args) throws Throwable {
		ExcelUtility excelUtility=new ExcelUtility();
		System.out.println(excelUtility.readLastStatus("C:\\test\\report.xls"));
	}
    
    public String readLastStatus(String filePath) throws Throwable{
    	FileInputStream excelFile = new FileInputStream(new File(filePath));
        Workbook workbook = new XSSFWorkbook(excelFile);
        Sheet datatypeSheet = workbook.getSheetAt(0);
        Row row=sheet.getRow(sheet.getLastRowNum());
		return row.getCell(1).getStringCellValue();
    }
    public void openExcelFile(String excelFilePath) throws Throwable{
    	FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
        workbook = (XSSFWorkbook) WorkbookFactory.create(inputStream);
    }
    
    public void openSheet(String sheetname){
    	sheet =workbook.getSheet(sheetname);
    }
    
	public void createSheet(String sheetName){
	   	workbook= new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file
    	createHelper = workbook.getCreationHelper();
		sheet= workbook.createSheet(sheetName);
	
	
	}
	public void populateRow(int rowNum,List<String> listValues){
		Row singleRow=sheet.createRow(rowNum);
		Iterator<String> iterator= listValues.iterator();
		for (int i = 0; i < listValues.size(); i++) {
			Cell cell=singleRow.createCell(i);
			cell.setCellValue(listValues.get(i));
		}
	}
	
	public void populateNextRow(List<String> listValues){
		int lastRowNum=sheet.getLastRowNum();
		populateRow(lastRowNum+1, listValues);
	}
	public void writeFile(String fileName) throws Exception{
		  FileOutputStream out = new FileOutputStream(new File(fileName)); 
          workbook.write(out); 
          out.close();
	}
	
	
	
	
	
}
