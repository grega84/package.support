package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

public class EmailUtility {
	 public static void fetch(String pop3Host, String storeType, String user,
		      String password) {
		      try {
		         // create properties field
		         Properties properties = new Properties();
		         properties.put("mail.store.protocol", "pop3");
		         properties.put("mail.pop3.host", pop3Host);
		         properties.put("mail.pop3.port", "995");
		         properties.put("mail.pop3.starttls.enable", "true");
		         Session emailSession = Session.getDefaultInstance(properties);
		         // emailSession.setDebug(true);

		         // create the POP3 store object and connect with the pop server
		         Store store = emailSession.getStore("pop3s");

		         store.connect(pop3Host, user, password);

		         // create the folder object and open it
		         Folder emailFolder = store.getFolder("INBOX");
		         emailFolder.open(Folder.READ_ONLY);

		         BufferedReader reader = new BufferedReader(new InputStreamReader(
			      System.in));

		         // retrieve the messages from the folder in an array and print it
		         Message[] messages = emailFolder.getMessages();
		         System.out.println("messages.length---" + messages.length);

		         for (int i = 0; i < messages.length; i++) {
		            Message message = messages[i];
		            System.out.println("---------------------------------");
//		            writePart(message);
		            String line = reader.readLine();
		            if ("YES".equals(line)) {
		               message.writeTo(System.out);
		            } else if ("QUIT".equals(line)) {
		               break;
		            }
		         }

		         // close the store and folder objects
		         emailFolder.close(false);
		         store.close();

		      } catch (NoSuchProviderException e) {
		         e.printStackTrace();
		      } catch (MessagingException e) {
		         e.printStackTrace();
		      } catch (IOException e) {
		         e.printStackTrace();
		      } catch (Exception e) {
		         e.printStackTrace();
		      }
		   }
	
	
	
	public static String filteredMailBySubject(String host, String storeType, String user,
		      String password,String subjectToFind) {
				String messageFinded=null;

		   
		      try {

		      //create properties field
		      Properties properties = new Properties();

		      properties.put("mail.pop3.host", host);
		      properties.put("mail.pop3.port", "995");
		      properties.put("mail.pop3.starttls.enable", "true");
		      Session emailSession = Session.getDefaultInstance(properties);
		  
		      //create the POP3 store object and connect with the pop server
//		      Store store = emailSession.getStore("pop3s");
		      Store store = emailSession.getStore("imaps");

		      store.connect(host, user, password);

		      //create the folder object and open it
		      Folder emailFolder = store.getFolder("INBOX");
		      emailFolder.open(Folder.READ_ONLY);
		      // retrieve the messages from the folder in an array and print it
		      Message[] messages = emailFolder.getMessages();
		      System.out.println("messages.length---" + messages.length);
		      boolean findedMessage=false;
		      for (int i =1 , n = messages.length ; i < n && !findedMessage; i++) {
		         int lastMail=n-i;
		    	  Message message = messages[lastMail];
//		    	  System.out.println(message.getReceivedDate());
//		         System.out.println("---------------------------------");
//		         System.out.println("Email Number " + (lastMail + 1));
//		         System.out.println("Subject: " + message.getSubject());
		         if(message.getSubject().contains(subjectToFind)){
//		        	 System.out.println("TROVATO......");
		        	 findedMessage=true;
		        	 messageFinded=message.getContent().toString();
		        	 System.out.println(messageFinded);
		         }
//		         System.out.println("From: " + message.getFrom()[0]);
//		         System.out.println("Text: " + message.getContent().toString());

		      }

		      //close the store and folder objects
		      emailFolder.close(false);
		      store.close();
		      } catch (NoSuchProviderException e) {
		         e.printStackTrace();
		      } catch (MessagingException e) {
		         e.printStackTrace();
		      } catch (Exception e) {
		         e.printStackTrace();
		      }
		      return messageFinded;

		   }
			public static String extractOTPFromMail(String host,  String mailStoreType, String username,String password){
				String message =filteredMailBySubject(host, mailStoreType, username, password,"Poste");
			      return message.split("codice ")[1].split(". Cordiali")[0];
			}
		   public static void main(String[] args) {

		      String host = "imap.gmail.com";// change accordingly
		      String mailStoreType = "pop3";
		      String username = "genny.rega@gmail.com";// change accordingly
		      String password = "BMW320Turing";// change accordingly

		      String message =filteredMailBySubject(host, mailStoreType, username, password,"Poste");
		      System.out.println(message.split("codice ")[1].split(". Cordiali")[0]);
		      
		   
		   }
}
