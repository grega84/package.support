package application;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import bean.ReceiverAccount;
import bean.SenderAccount;
public class AfterTest  {
	public static void main(String[] args) throws Exception, Throwable {
		ZipFiles zipFiles= new ZipFiles();
		Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH_mm");
        System.out.println( sdf.format(cal.getTime()) );
        String zipPathFinal=""+sdf.format(cal.getTime())+".zip";
        String filename="cucumber-html-reports"+zipPathFinal;
		zipFiles.compressFilesAndFolders(args[0], args[1]+zipPathFinal);
		
		SendMailBySite sendMailBySite= new SendMailBySite();
		SenderAccount senderAccount= new SenderAccount();
		
		senderAccount.setUserName(args[2]);
		senderAccount.setPassword(args[3]);
		String[] receiver=args[4].split(";");
		List<ReceiverAccount> receiverAccounts= new ArrayList<>();
		
		for (int i = 0; i < receiver.length; i++) {
			ReceiverAccount receiverAccount= new ReceiverAccount();
			receiverAccount.setReceiverAccount(receiver[i]);
			receiverAccounts.add(receiverAccount);
		}
		DropBoxUtils dropBoxUtils= new DropBoxUtils();
		dropBoxUtils.createFolder("/prisma");
		dropBoxUtils.createFolder("/prisma/PostePortale");
		dropBoxUtils.createFolder("/prisma/PostePortale/ListaMovimenti");
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf3 = new SimpleDateFormat("dd_MM_YYYY");
        System.out.println( sdf3.format(cal.getTime()) );
        String dataFolder=sdf3.format(cal.getTime());
        String remotePath="/prisma/PostePortale/ListaMovimenti/"+dataFolder;
        dropBoxUtils.createFolder(remotePath);
		dropBoxUtils.uploadFile(args[1]+zipPathFinal, remotePath+"/"+filename);
		String sharedLink=dropBoxUtils.shareFilePath(remotePath+"/"+filename);
		System.out.println(sharedLink);
		
		SimpleDateFormat sdf2= new SimpleDateFormat("dd_MM_YYYY-HH_mm");
		System.out.println("testCorretto");
		System.out.println(sharedLink);
		
		sendMailBySite.mailSent(senderAccount, receiverAccounts, "Lista Movimenti Execution Test: "+sdf2.format(cal.getTime()), "Ciao,"+System.lineSeparator()+
				"di seguito il link con il report del test"+System.lineSeparator()+sharedLink+System.lineSeparator()+
				"Per poter visualizzare il report e' necessario scaricare il file zip estrarlo ed aprire la pagina 'overview-features.html'"+System.lineSeparator()+"Saluti");
	}
}
