package application;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.filerequests.CreateBuilder;
import com.dropbox.core.v2.files.CreateFolderErrorException;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.FolderMetadata;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.sharing.CreateSharedLinkWithSettingsErrorException;
import com.dropbox.core.v2.sharing.RequestedVisibility;
import com.dropbox.core.v2.sharing.SharedLinkMetadata;
import com.dropbox.core.v2.sharing.SharedLinkSettings;
import com.dropbox.core.v2.teamlog.CreateFolderDetails;
import com.dropbox.core.v2.users.FullAccount;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;

public class DropBoxUtils {
    private static final String ACCESS_TOKEN = "eYtUC_q3XlAAAAAAAAAAJp1icxvjD4Zaa7Q7syEogPnhe5UIjTyPSmcAKFY0oLMN";
    private DbxRequestConfig config;
    private DbxClientV2 client;
    public DropBoxUtils(){
    	config = new DbxRequestConfig("dropbox/java-tutorial", "en_US");
        client = new DbxClientV2(config, ACCESS_TOKEN);

    }
    public void createFolder(String newFolder){
    	try {
            FolderMetadata folder = client.files().createFolder(newFolder);
        } catch (CreateFolderErrorException err) {
            if (err.errorValue.isPath() && err.errorValue.getPathValue().isConflict()) {
                System.out.println("Something already exists at the path.");
            } else {
                System.out.print("Some other CreateFolderErrorException occurred...");
                System.out.print(err.toString());
            }
        } catch (Exception err) {
            System.out.print("Some other Exception occurred...");
            System.out.print(err.toString());
        }
    }
    public void uploadFile(String localFile,String remoteFilePath) throws Exception, IOException{
    	 try (InputStream in = new FileInputStream(localFile)) {
             FileMetadata metadata = client.files().uploadBuilder(remoteFilePath)
                 .uploadAndFinish(in);
    	 }catch (Exception err) {
            
             System.out.print(err.toString());
         }
    }
    public String shareFilePath(String remoteFilePath) throws Exception, DbxException{
    	 SharedLinkMetadata slm = client.sharing().createSharedLinkWithSettings(remoteFilePath, SharedLinkSettings.newBuilder().withRequestedVisibility(RequestedVisibility.PUBLIC).build());
         return(slm.getUrl());
    }
    
}