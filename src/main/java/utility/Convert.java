package utility;

import java.util.List;

import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import bean.ReceiverAccount;

public class Convert {
	public static Address[] receiverAccountsToAddresses(List<ReceiverAccount> receivers) throws Exception{
//		List<Address> addresses = new ;
		Address[] addresses= new InternetAddress[receivers.size()];
		for (int i=0;i<receivers.size();i++) {
			addresses[i]=new InternetAddress(receivers.get(i).getReceiverAccount());
		}
		return addresses;
				
	}
}
